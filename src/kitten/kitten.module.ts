import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { KittenController } from './kitten.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KittenEntity } from './kitten.entity';
import { UserEntity } from '../user/user.entity';
import { KittenService } from './kitten.service';
import { AuthMiddleware } from '../user/auth.middleware';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([KittenEntity,UserEntity]), UserModule],
  providers: [KittenService],
  controllers: [
    KittenController
  ]
})
export class KittenModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes(
        {path: 'kittens', method: RequestMethod.GET},
        {path: 'kittens', method: RequestMethod.DELETE},
        {path: 'kittens', method: RequestMethod.PUT},
        {path: 'kittens', method: RequestMethod.POST});
  }
}
