import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository, DeleteResult } from 'typeorm';
import { KittenEntity } from './kitten.entity';
import { UserEntity } from '../user/user.entity';
import { CreateKittenDto } from './dto';

import {KittenRO, KittensRO} from './kitten.interface';


@Injectable()
export class KittenService {
  constructor(
    @InjectRepository(KittenEntity)
    private readonly kittenRepository: Repository<KittenEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async findAll(): Promise<KittensRO> {
  
    const qb = await getRepository(KittenEntity)
      .createQueryBuilder('kitten')

    const kitten = await qb.getMany();
    return {kitten};
  }


//   async findOne(where): Promise<KittenRO> {
//     const kitten = await this.kittenRepository.findOne(where);
//     return {kitten};
//   }

//   CREATE
  async create(dto: CreateKittenDto): Promise<KittenEntity> {

    const {id, name, race,color,age} = dto;


    let kitten = new KittenEntity();
    kitten.id = id;
    kitten.name = name;
    kitten.race = race;
    kitten.color =color;
    kitten.age = age;

    // kitten.id = kittenData.id;
    // kitten.name = kittenData.name;
    // kitten.race = kittenData.race;
    // kitten.color = kittenData.color;
    // kitten.age = kittenData.age;

    const newKitten = await this.kittenRepository.save(kitten);

    return newKitten;
  }

// UPDATE
  async update(dto: CreateKittenDto): Promise<KittenRO> {

    const {id, name, race,color,age} = dto;

    let updtkitten = new KittenEntity();
    updtkitten.id = id;
    updtkitten.name = name;
    updtkitten.race = race;
    updtkitten.color =color;
    updtkitten.age = age;

    let toUpdate = await this.kittenRepository.findOne({ id: id});//{ where: { id: userId } }
    let updated = Object.assign(toUpdate, updtkitten);
    const kitten = await this.kittenRepository.save(updated);
    return {kitten};
  }

// DELETE
  async delete(id: number): Promise<DeleteResult> {
    return await this.kittenRepository.delete({ id: id});
  }
}
