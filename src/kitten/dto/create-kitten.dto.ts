import { IsNotEmpty } from "class-validator";

export class CreateKittenDto {

  @IsNotEmpty()
  readonly id: number;

  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty()
  readonly color: number; //0-blanc 1-roux 2-noir

  @IsNotEmpty()
  readonly race: number;//0-siamois 1-sphynx 2-européen

  @IsNotEmpty()
  readonly age: string;



}
