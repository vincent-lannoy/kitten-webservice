import { UserData } from '../user/user.interface';
import { KittenEntity } from './kitten.entity';


interface KittenData {
 id?: number;
 name: string;
 color: number; //0-blanc 1-roux 2-noir
  race: number;//0-siamois 2-sphynx 3-européen
 age: string;

}

export interface KittenRO {
  kitten: KittenEntity;
}

export interface KittensRO {
  kitten: KittenEntity[];
}

