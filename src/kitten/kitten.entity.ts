import { Entity, PrimaryGeneratedColumn, Column} from 'typeorm';
import { UserEntity } from '../user/user.entity';

@Entity('kitten')
export class KittenEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  color: number;

  @Column()
  race: number;
  
  @Column()
  age: string;

}