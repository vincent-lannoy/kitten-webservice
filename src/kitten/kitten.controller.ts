import {Get, Post, Body, Put, Delete, Query, Param, Controller, ValidationPipe, UsePipes} from '@nestjs/common';
import { Request } from 'express';
import { KittenService } from './kitten.service';
import { CreateKittenDto } from './dto';
import { KittensRO, KittenRO } from './kitten.interface';

import {
  ApiUseTags,
  ApiBearerAuth,
  ApiResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { KittenEntity } from './kitten.entity';

@ApiBearerAuth()
@ApiUseTags('kittens')
@Controller()
export class KittenController {

  constructor(private readonly kittenService: KittenService) {}
//GET
  @ApiOperation({ title: 'Get all kittens' })
  @ApiResponse({ status: 200, description: 'Return all kittens.'})
  @Get('kittens')
  async findAll(): Promise<KittensRO> {
    return await this.kittenService.findAll();
  }

//   @Get(':slug')
//   async findOne(@Param('slug') slug): Promise<KittenRO> {
//     return await this.kittenService.findOne({slug});
//   }

//   @Get(':slug/comments')
//   async findComments(@Param('slug') slug): Promise<CommentsRO> {
//     return await this.kittenService.findComments(slug);
//   }


  //CREATE 
  @ApiOperation({ title: 'Create kitten' })
  @ApiResponse({ status: 201, description: 'The kitten has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @UsePipes(new ValidationPipe())
  @Post('kittens')
  async create(@Body('kitten') kittenData: CreateKittenDto) {
    console.log("---------------LOLOL---------");
    console.log(kittenData);
    return this.kittenService.create(kittenData);
  }

  //UPDATE
  @ApiOperation({ title: 'Update kitten' })
  @ApiResponse({ status: 201, description: 'The kitten has been successfully updated.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Put('kittens')
  async update( @Body('kitten') kittenData: CreateKittenDto) {

    return this.kittenService.update(kittenData);
  }

//   DELETE
  @ApiOperation({ title: 'Delete kitten' })
  @ApiResponse({ status: 201, description: 'The kitten has been successfully deleted.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Delete('kittens')
  async delete(@Body('kitten') kittenData: KittenEntity) {
    return this.kittenService.delete(kittenData.id);
  }

}