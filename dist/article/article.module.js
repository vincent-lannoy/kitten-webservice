"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const kitten_controller_1 = require("./kitten.controller");
const typeorm_1 = require("@nestjs/typeorm");
const kitten_entity_1 = require("./kitten.entity");
const comment_entity_1 = require("./comment.entity");
const user_entity_1 = require("../user/user.entity");
const follows_entity_1 = require("../profile/follows.entity");
const kitten_service_1 = require("./kitten.service");
const auth_middleware_1 = require("../user/auth.middleware");
const user_module_1 = require("../user/user.module");
let KittenModule = class KittenModule {
    configure(consumer) {
        consumer
            .apply(auth_middleware_1.AuthMiddleware)
            .forRoutes({ path: 'kittens/feed', method: common_1.RequestMethod.GET }, { path: 'kittens', method: common_1.RequestMethod.POST }, { path: 'kittens/:slug', method: common_1.RequestMethod.DELETE }, { path: 'kittens/:slug', method: common_1.RequestMethod.PUT }, { path: 'kittens/:slug/comments', method: common_1.RequestMethod.POST }, { path: 'kittens/:slug/comments/:id', method: common_1.RequestMethod.DELETE }, { path: 'kittens/:slug/favorite', method: common_1.RequestMethod.POST }, { path: 'kittens/:slug/favorite', method: common_1.RequestMethod.DELETE });
    }
};
KittenModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([kitten_entity_1.KittenEntity, comment_entity_1.Comment, user_entity_1.UserEntity, follows_entity_1.FollowsEntity]), user_module_1.UserModule],
        providers: [kitten_service_1.KittenService],
        controllers: [
            kitten_controller_1.KittenController
        ]
    })
], KittenModule);
exports.KittenModule = KittenModule;
//# sourceMappingURL=kitten.module.js.map