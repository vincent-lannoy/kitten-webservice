"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
const common_1 = require("@nestjs/common");
const kitten_service_1 = require("./kitten.service");
const dto_1 = require("./dto");
const user_decorator_1 = require("../user/user.decorator");
const swagger_1 = require("@nestjs/swagger");
let KittenController = class KittenController {
    constructor(kittenService) {
        this.kittenService = kittenService;
    }
    findAll(query) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.kittenService.findAll(query);
        });
    }
    findOne(slug) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.kittenService.findOne({ slug });
        });
    }
    findComments(slug) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.kittenService.findComments(slug);
        });
    }
    create(userId, kittenData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.kittenService.create(userId, kittenData);
        });
    }
    update(params, kittenData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.kittenService.update(params.slug, kittenData);
        });
    }
    delete(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.kittenService.delete(params.slug);
        });
    }
    createComment(slug, commentData) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.kittenService.addComment(slug, commentData);
        });
    }
    deleteComment(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { slug, id } = params;
            return yield this.kittenService.deleteComment(slug, id);
        });
    }
    favorite(userId, slug) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.kittenService.favorite(userId, slug);
        });
    }
    unFavorite(userId, slug) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.kittenService.unFavorite(userId, slug);
        });
    }
    getFeed(userId, query) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.kittenService.findFeed(userId, query);
        });
    }
};
__decorate([
    swagger_1.ApiOperation({ title: 'Get all kittens' }),
    swagger_1.ApiResponse({ status: 200, description: 'Return all kittens.' }),
    common_1.Get(),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "findAll", null);
__decorate([
    common_1.Get(':slug'),
    __param(0, common_1.Param('slug')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "findOne", null);
__decorate([
    common_1.Get(':slug/comments'),
    __param(0, common_1.Param('slug')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "findComments", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Create kitten' }),
    swagger_1.ApiResponse({ status: 201, description: 'The kitten has been successfully created.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Post(),
    __param(0, user_decorator_1.User('id')), __param(1, common_1.Body('kitten')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, dto_1.CreateKittenDto]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "create", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Update kitten' }),
    swagger_1.ApiResponse({ status: 201, description: 'The kitten has been successfully updated.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Put(':slug'),
    __param(0, common_1.Param()), __param(1, common_1.Body('kitten')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.CreateKittenDto]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "update", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Delete kitten' }),
    swagger_1.ApiResponse({ status: 201, description: 'The kitten has been successfully deleted.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Delete(':slug'),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "delete", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Create comment' }),
    swagger_1.ApiResponse({ status: 201, description: 'The comment has been successfully created.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Post(':slug/comments'),
    __param(0, common_1.Param('slug')), __param(1, common_1.Body('comment')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, typeof (_a = typeof dto_1.CreateCommentDto !== "undefined" && dto_1.CreateCommentDto) === "function" ? _a : Object]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "createComment", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Delete comment' }),
    swagger_1.ApiResponse({ status: 201, description: 'The kitten has been successfully deleted.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Delete(':slug/comments/:id'),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "deleteComment", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Favorite kitten' }),
    swagger_1.ApiResponse({ status: 201, description: 'The kitten has been successfully favorited.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Post(':slug/favorite'),
    __param(0, user_decorator_1.User('id')), __param(1, common_1.Param('slug')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "favorite", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Unfavorite kitten' }),
    swagger_1.ApiResponse({ status: 201, description: 'The kitten has been successfully unfavorited.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Delete(':slug/favorite'),
    __param(0, user_decorator_1.User('id')), __param(1, common_1.Param('slug')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "unFavorite", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Get kitten feed' }),
    swagger_1.ApiResponse({ status: 200, description: 'Return kitten feed.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Get('feed'),
    __param(0, user_decorator_1.User('id')), __param(1, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], KittenController.prototype, "getFeed", null);
KittenController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('kittens'),
    common_1.Controller('kittens'),
    __metadata("design:paramtypes", [kitten_service_1.KittenService])
], KittenController);
exports.KittenController = KittenController;
//# sourceMappingURL=kitten.controller.js.map