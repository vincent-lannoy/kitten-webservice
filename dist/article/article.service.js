"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const kitten_entity_1 = require("./kitten.entity");
const comment_entity_1 = require("./comment.entity");
const user_entity_1 = require("../user/user.entity");
const follows_entity_1 = require("../profile/follows.entity");
const slug = require('slug');
let KittenService = class KittenService {
    constructor(kittenRepository, commentRepository, userRepository, followsRepository) {
        this.kittenRepository = kittenRepository;
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.followsRepository = followsRepository;
    }
    findAll(query) {
        return __awaiter(this, void 0, void 0, function* () {
            const qb = yield typeorm_2.getRepository(kitten_entity_1.KittenEntity)
                .createQueryBuilder('kitten')
                .leftJoinAndSelect('kitten.author', 'author');
            qb.where("1 = 1");
            if ('tag' in query) {
                qb.andWhere("kitten.tagList LIKE :tag", { tag: `%${query.tag}%` });
            }
            if ('author' in query) {
                const author = yield this.userRepository.findOne({ username: query.author });
                qb.andWhere("kitten.authorId = :id", { id: author.id });
            }
            if ('favorited' in query) {
                const author = yield this.userRepository.findOne({ username: query.favorited });
                const ids = author.favorites.map(el => el.id);
                qb.andWhere("kitten.authorId IN (:ids)", { ids });
            }
            qb.orderBy('kitten.created', 'DESC');
            const kittensCount = yield qb.getCount();
            if ('limit' in query) {
                qb.limit(query.limit);
            }
            if ('offset' in query) {
                qb.offset(query.offset);
            }
            const kittens = yield qb.getMany();
            return { kittens, kittensCount };
        });
    }
    findFeed(userId, query) {
        return __awaiter(this, void 0, void 0, function* () {
            const _follows = yield this.followsRepository.find({ followerId: userId });
            const ids = _follows.map(el => el.followingId);
            const qb = yield typeorm_2.getRepository(kitten_entity_1.KittenEntity)
                .createQueryBuilder('kitten')
                .where('kitten.authorId IN (:ids)', { ids });
            qb.orderBy('kitten.created', 'DESC');
            const kittensCount = yield qb.getCount();
            if ('limit' in query) {
                qb.limit(query.limit);
            }
            if ('offset' in query) {
                qb.offset(query.offset);
            }
            const kittens = yield qb.getMany();
            return { kittens, kittensCount };
        });
    }
    findOne(where) {
        return __awaiter(this, void 0, void 0, function* () {
            const kitten = yield this.kittenRepository.findOne(where);
            return { kitten };
        });
    }
    addComment(slug, commentData) {
        return __awaiter(this, void 0, void 0, function* () {
            let kitten = yield this.kittenRepository.findOne({ slug });
            const comment = new comment_entity_1.Comment();
            comment.body = commentData.body;
            kitten.comments.push(comment);
            yield this.commentRepository.save(comment);
            kitten = yield this.kittenRepository.save(kitten);
            return { kitten };
        });
    }
    deleteComment(slug, id) {
        return __awaiter(this, void 0, void 0, function* () {
            let kitten = yield this.kittenRepository.findOne({ slug });
            const comment = yield this.commentRepository.findOne(id);
            const deleteIndex = kitten.comments.findIndex(_comment => _comment.id === comment.id);
            if (deleteIndex >= 0) {
                const deleteComments = kitten.comments.splice(deleteIndex, 1);
                yield this.commentRepository.delete(deleteComments[0].id);
                kitten = yield this.kittenRepository.save(kitten);
                return { kitten };
            }
            else {
                return { kitten };
            }
        });
    }
    favorite(id, slug) {
        return __awaiter(this, void 0, void 0, function* () {
            let kitten = yield this.kittenRepository.findOne({ slug });
            const user = yield this.userRepository.findOne(id);
            const isNewFavorite = user.favorites.findIndex(_kitten => _kitten.id === kitten.id) < 0;
            if (isNewFavorite) {
                user.favorites.push(kitten);
                kitten.favoriteCount++;
                yield this.userRepository.save(user);
                kitten = yield this.kittenRepository.save(kitten);
            }
            return { kitten };
        });
    }
    unFavorite(id, slug) {
        return __awaiter(this, void 0, void 0, function* () {
            let kitten = yield this.kittenRepository.findOne({ slug });
            const user = yield this.userRepository.findOne(id);
            const deleteIndex = user.favorites.findIndex(_kitten => _kitten.id === kitten.id);
            if (deleteIndex >= 0) {
                user.favorites.splice(deleteIndex, 1);
                kitten.favoriteCount--;
                yield this.userRepository.save(user);
                kitten = yield this.kittenRepository.save(kitten);
            }
            return { kitten };
        });
    }
    findComments(slug) {
        return __awaiter(this, void 0, void 0, function* () {
            const kitten = yield this.kittenRepository.findOne({ slug });
            return { comments: kitten.comments };
        });
    }
    create(userId, kittenData) {
        return __awaiter(this, void 0, void 0, function* () {
            let kitten = new kitten_entity_1.KittenEntity();
            kitten.title = kittenData.title;
            kitten.description = kittenData.description;
            kitten.slug = this.slugify(kittenData.title);
            kitten.tagList = kittenData.tagList || [];
            kitten.comments = [];
            const newKitten = yield this.kittenRepository.save(kitten);
            const author = yield this.userRepository.findOne({ where: { id: userId } });
            if (Array.isArray(author.kittens)) {
                author.kittens.push(kitten);
            }
            else {
                author.kittens = [kitten];
            }
            yield this.userRepository.save(author);
            return newKitten;
        });
    }
    update(slug, kittenData) {
        return __awaiter(this, void 0, void 0, function* () {
            let toUpdate = yield this.kittenRepository.findOne({ slug: slug });
            let updated = Object.assign(toUpdate, kittenData);
            const kitten = yield this.kittenRepository.save(updated);
            return { kitten };
        });
    }
    delete(slug) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.kittenRepository.delete({ slug: slug });
        });
    }
    slugify(title) {
        return slug(title, { lower: true }) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36);
    }
};
KittenService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(kitten_entity_1.KittenEntity)),
    __param(1, typeorm_1.InjectRepository(comment_entity_1.Comment)),
    __param(2, typeorm_1.InjectRepository(user_entity_1.UserEntity)),
    __param(3, typeorm_1.InjectRepository(follows_entity_1.FollowsEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], KittenService);
exports.KittenService = KittenService;
//# sourceMappingURL=kitten.service.js.map