
## Installation

-cloner les repo
    
    git clone git@bitbucket.org:vincent-lannoy/kitten-project.git


    
-installer les dépendances
    
    npm install


## base de données

    creer une bdd SQL local 
    
Parametrer la bdd SQL dans ormconfig.json

    {
      "type": "mysql",
      "host": "localhost",
      "port": 3306,
      "username": "your-mysql-username",
      "password": "your-mysql-password",
      "database": "kittens",
      "entities": ["src/**/**.entity{.ts,.js}"],
      "synchronize": true
    }
    


 
   